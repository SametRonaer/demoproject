import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:second_bloc_demo/business_logic/cubit/counter_cubit.dart';
import 'package:second_bloc_demo/presentation/screens/second_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.title, required this.color}) : super(key: key);
  static const routeName = "/home_screen";
  final String title;
  final Color color;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            BlocConsumer<CounterCubit, CounterState>(
              listener: (context, state) {
                // TODO: implement listener
                 if (state.wasIncremented!) {
                 Scaffold.of(context)
                     .showSnackBar(SnackBar(content: Text("Increment")));
               } else {
                 Scaffold.of(context)
                     .showSnackBar(SnackBar(content: Text("Decrement")));
               }
              },
              builder: (context, state) {
                return Text(
                  '${state.counterValue.toString()}',
                  style: Theme.of(context).textTheme.headline4,
                );
              },
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  FloatingActionButton(
                    onPressed: () {
                      BlocProvider.of<CounterCubit>(context).decrement();
                      //context.read()
                    },
                    tooltip: 'Decrement',
                    child: Icon(Icons.remove),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      BlocProvider.of<CounterCubit>(context).increment();
                    },
                    tooltip: 'Increment',
                    child: Icon(Icons.add),
                  ),
                ],
              ),
            ),
            MaterialButton(
              color: widget.color,
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (constext) => SecondScreen(title: "Second Screen", color: Colors.redAccent)));
              }, child: Text("Go to second screen"),)
          ],
        ),
      ),
    );
  }
}
