import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'business_logic/cubit/counter_cubit.dart';
import 'presentation/screens/home_screen.dart';
import 'presentation/screens/second_screen.dart';
import 'presentation/screens/third_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final CounterCubit _counterCubit = CounterCubit();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterCubit>(
      create: (context) => CounterCubit(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: {
          HomeScreen.routeName: (context) => BlocProvider.value(
                value: _counterCubit,
                child:
                    HomeScreen(title: "Home Screen", color: Colors.blueAccent),
              ),
          SecondScreen.routeName: (context) => BlocProvider.value(
                value: _counterCubit,
                child: SecondScreen(
                    title: "Second Screen", color: Colors.redAccent),
              ),
          ThirdScreen.routeName: (context) => BlocProvider.value(
                value: _counterCubit,
                child: ThirdScreen(
                    title: "Third Screen", color: Colors.greenAccent),
              ),
        },
        initialRoute: HomeScreen.routeName,
      ),
    );
  }
}
